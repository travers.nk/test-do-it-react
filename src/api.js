
export default function(url) {

    function generateUri(args) {
        let uri = '?';
        let argCount = 0;
        for (let key in args) {
            if (args.hasOwnProperty(key)) {
                if (argCount++) {
                    uri += '&';
                }
                uri += `${encodeURIComponent(key)}=${encodeURIComponent(args[key])}`;
            }
        }
        return uri;
    }

    function ajax(method, url, args, opts) {
        // Return the promise
        return new Promise(function(resolve, reject) {

            // Instantiates the XMLHttpRequest
            const client = new XMLHttpRequest();
            const uri = url;
            let data;

            if (args) {
                if (method === 'GET') {
                    client.open(method, `${uri}${generateUri(args)}`);
                } else {
                    client.open(method, uri);
                    client.setRequestHeader('Content-Type', 'application/json');
                    data = JSON.stringify(args)
                }
            } else {
                client.open(method, url);
            }

            if (opts && opts.headers) {
                let headers = opts.headers;
                for (let opt in headers) {
                    if (headers.hasOwnProperty(opt)) {
                        client.setRequestHeader(opt, headers[opt]);
                    }
                }
            }

            if (data) {
                client.send(data);
            } else {
                client.send();
            }

            client.onload = function() {
                if (this.status >= 200 && this.status < 300) {
                    let response = this.response;
                    // Performs the function "resolve" when this.status is equal to 2xx
                    resolve((typeof response === 'string') ? JSON.parse(response) : response);
                } else {
                    // Performs the function "reject" when this.status is different than 2xx
                    reject({'status': this.statusText, 'errors': this.response});
                }
            };
            client.onerror = function() {
                reject(this.statusText);
            };
        });
    }

    // Adapter pattern
    return {
        'get': (args, opts) => ajax('GET', url, args, opts),
        'post': (args, opts) => ajax('POST', url, args, opts),
        'put': (args, opts) => ajax('PUT', url, args, opts),
        'delete': (args, opts) => ajax('DELETE', url, args, opts)
    };
}
