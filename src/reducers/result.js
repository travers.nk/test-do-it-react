const result = (store = [], action) => {
    switch (action.type) {

        case 'ADD_LOCATION_SUCCESS':
            return action.payload.message;
        case 'ADD_LOCATION_FAIL':
            return action.payload.status;
        case 'LOGIN_SUCCESS':
            return action.payload.message;
        case 'LOGIN_FAIL':
            return action.payload.status;
        case 'REGISTER_SUCCESS':
            return action.payload.message;
        case 'REGISTER_FAIL':
            return action.payload.status;
        case 'FETCH_USERS_FAIL':
            return action.payload.status;
        case 'CLEAR_RESULT':
            return '';
        default:
            return store;
    }
};

export default result;