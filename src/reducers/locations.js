const locations = (store = [], action) => {

    switch (action.type) {

        case 'ADD_LOCATION_SUCCESS':
            return [
                ...store,
                {
                    id: action.id,
                    name: action.payload.data.name,
                    lat: action.payload.data.lat,
                    lng: action.payload.data.lng,
                    userId: action.payload.data.user_id,
                    type: action.payload.data.type
                }
            ];
        case 'CLEAR':
            return [];
        default:
            return store;
    }
};

export default locations;