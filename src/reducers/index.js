import {combineReducers} from 'redux';
import users from './users';
import map from "./map";
import user from "./user";
import result from "./result";
import locations from "./locations";
import errors from "./errors";
import authentication from "./authentication";

const todoApp = combineReducers({
    users,
    map,
    user,
    result,
    locations,
    errors,
    authentication

});

export default todoApp
