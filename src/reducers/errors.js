const errors = (store = [], action) => {

    switch (action.type) {
        case 'ADD_LOCATION_FAIL':
            return JSON.parse(action.payload.errors);
        case 'LOGIN_FAIL':
            return JSON.parse(action.payload.errors);
        case 'REGISTER_FAIL':
            return JSON.parse(action.payload.errors);
        case 'FETCH_USERS_FAIL':
            return JSON.parse(action.payload.errors);
        case 'CLEAR_ERRORS':
            return [];
        default:
            return store;
    }
};

export default errors;