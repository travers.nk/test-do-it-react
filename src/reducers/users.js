const users = (state = [], action) => {

    switch (action.type) {
        case 'ADD_USER':
            return [
                ...state,
                {
                    id: action.id,
                    name: action.name
                }
            ];
        case 'FETCH_USER_SUCCESS':
            return action.payload;

        default:
            return state
    }
};

export default users
