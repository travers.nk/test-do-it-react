const map = (store = [], action) => {
    switch (action.type) {
        case 'SET_LOCATION':
            return [
                action.data,
            ];
        case 'CLEAR_MAP':
            return [];

        default:
            return store;
    }
};

export default map;
