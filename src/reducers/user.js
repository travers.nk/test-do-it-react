const user = (store = [], action) => {
    switch (action.type) {
        case 'FETCH_USER_LOCATION_SUCCESS':
            return [
                action.payload
            ];
        case 'CLEAR':
            return [];
        default:
            return store
    }
};

export default user