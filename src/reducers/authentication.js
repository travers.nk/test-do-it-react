
let user = JSON.parse(localStorage.getItem('userLogged'));
const initialState = user ? { loggedIn: true, user } : {};

const authentication = (store = initialState, action) => {

    switch (action.type) {
        case 'LOGIN_SUCCESS':
            return {
                loggedIn: action.payload.success,
                userId: action.payload.userId,
                token: action.payload.token
            };
        case 'LOGIN_FAIL':
            return {};
        case 'LOGOUT_SUCCESS':
            return {};
        case 'REGISTER_SUCCESS':
            return {
                loggedIn: action.payload.success,
                userId: action.payload.userId,
                token: action.payload.token
            };
        case 'REGISTER_FAIL':
            return {};
        default:
            return store
    }
};

export default authentication