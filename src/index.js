import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import {applyMiddleware, createStore} from 'redux';
import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom';
import app from './reducers';
import {createLogger} from 'redux-logger'
import thunk from 'redux-thunk';
import {compose} from "recompose";


let store = createStore(app,
    compose(
        applyMiddleware(createLogger()),
        applyMiddleware(thunk)
    )
);

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();

//"https://floating-caverns-80163.herokuapp.com/api/v1/"