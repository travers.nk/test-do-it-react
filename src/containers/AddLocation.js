import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addLocation, clearResult, clearErrors } from '../actions'
import PropTypes from "prop-types";
import { FormGroup, FormControl, Button } from 'react-bootstrap';
import InfoPanel from "../components/common/InfoPanel";


class AddLocation extends Component {

    constructor (props) {
        super(props);
        this.locationSubmit = this.locationSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.state = {
            mapLocation: {
                name: '',
                UserId: '',
                lat: '',
                lon: '',
            },
            allowedMarker: this.props.allowedMarker,
            success: ''
        };
    };

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps){
            this.setState({
                mapLocation: {
                    name: nextProps.mapLocation.name,
                    UserId: nextProps.mapLocation.userId,
                    lat: nextProps.mapLocation.lat,
                    lng: nextProps.mapLocation.lng,
                },
                success: nextProps.result
            });
        }
    }

    handleChange(event) {
       this.setState({
           mapLocation: {
               name: event.target.value,
               UserId: this.props.mapLocation.userId,
               lat: this.props.mapLocation.lat,
               lng: this.props.mapLocation.lng,
           },
       });
    }

    handleClick(event) {
        this.props.clearResult();
        this.props.clearErrors();
    }

    locationSubmit(e){
        e.preventDefault();

        //if (!this.state.mapLocation.name){
        //    return
        //}
        this.props.addLocation(this.state.mapLocation);
    }

    render(){
        return (
            <div>
                <InfoPanel success={this.state.success} errors={this.props.errors} />
                <FormGroup>
                    <form
                        onSubmit={this.locationSubmit}
                    >
                        <FormControl
                            value={this.state.mapLocation.name ? this.state.mapLocation.name : ''}
                            onChange={this.handleChange} onClick={this.handleClick}
                        />
                        <br/>
                        <Button bsStyle="success" type="submit">
                            Add Location
                        </Button>
                    </form>
                </FormGroup>
            </div>
        )
    }
}

AddLocation.propTypes = {
    name: PropTypes.string.isRequired
};

const mapStateToProps = store => ({
    mapLocation: store.map[0] ? store.map[0] : [],
    result: store.result,
    errors: store.errors,
    name: ''
});

const mapDispatchToProps = dispatch => ({
    addLocation: (mapLocation) => dispatch(addLocation(mapLocation)),
    clearResult: () => dispatch(clearResult()),
    clearErrors: () => dispatch(clearErrors())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddLocation);