export function authHeader() {
    // return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('userLogged'));
    if (user && user.token) {
        return { 'X-Requested-With': user.token };
    } else {
        return {};
    }
}