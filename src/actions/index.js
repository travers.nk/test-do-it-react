import http from "../api";
import config from "../config.json";
import { authHeader } from "./authHeader";
import { history } from './history';

let nextUserId = 0;
let nextLocationId = 0;

export const addUser = name => {
    return {
        type: 'ADD_USER',
        id: nextUserId++,
        name
    }
};

export const setLocation = data => {
    return {
        type: 'SET_LOCATION',
        data
    }
};

export const clearMap = () => {
    return {
        type: 'CLEAR_MAP',
    }
};

export const clearResult = () => {
    return {
        type: 'CLEAR_RESULT',
    }
};

export const clearErrors = () => {
    return {
        type: 'CLEAR_ERRORS',
    }
};

const addLocationSuccess = result => ({
    type: 'ADD_LOCATION_SUCCESS',
    payload: result,
    id: nextLocationId++
});

const addLocationFail = result => ({
    type: 'ADD_LOCATION_FAIL',
    payload: result,
});

export const addLocation = data => dispatch =>
    http(config.url + 'location')
        .post(data, {headers: authHeader()})
        .then(result => {
            dispatch(addLocationSuccess(result));
            dispatch(clearMap());
        })
        .catch(err => {
            dispatch(addLocationFail(err));
        });

const fetchUsersSuccess = users => ({
    type: 'FETCH_USER_SUCCESS',
    payload: users
});

const fetchUsersFail = users => ({
    type: 'FETCH_USERS_FAIL',
    payload: users
});

export const fetchUsers = () => dispatch =>
    http(config.url)
        .get(null, {headers: authHeader()})
        .then(users => dispatch(fetchUsersSuccess(users)))
        .catch(err => {
            dispatch(fetchUsersFail(err));
        });

const fetchUserLocationSuccess = user => ({
    type: 'FETCH_USER_LOCATION_SUCCESS',
    payload: user
});

export const fetchUserLocation = userId => dispatch =>
    http(config.url + 'user/' + userId + '/location')
        .get(null, {headers: authHeader()})
        .then(user => dispatch(fetchUserLocationSuccess(user)))
        .catch(console.log);


const loginSuccess = user => ({
    type: 'LOGIN_SUCCESS',
    payload: user
});

const loginFail = result => ({
    type: 'LOGIN_FAIL',
    payload: result
});

const logoutSuccess = () => ({
    type: 'LOGOUT_SUCCESS'
});

export const login = (data) => dispatch =>
    http(config.url + 'auth/login')
        .post(data)
        .then(response => {
            if (response && response.token) {
                localStorage.setItem('userLogged', JSON.stringify(response));
                dispatch(loginSuccess(response));
                history.push('/');
            }
        })
        .catch(err => {
            dispatch(loginFail(err));
        });


export const logout = () => dispatch => {
    localStorage.removeItem('userLogged');
    dispatch(logoutSuccess());
};

const registerFail = result => ({
    type: 'REGISTER_FAIL',
    payload: result
});

const registerSuccess = (result) => ({
    type: 'REGISTER_SUCCESS',
    payload: result
});

export const register = (data) => dispatch =>
    http(config.url + 'auth/register')
        .post(data)
        .then(response => {
            if (response) {
                localStorage.setItem('userLogged', JSON.stringify(response));
                dispatch(registerSuccess(response));
                history.push('/');
            }
        })
        .catch(err => {
            dispatch(registerFail(err));
        });

