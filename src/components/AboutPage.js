import React, {Component} from 'react';
import About from "./common/About";
import Layout from "./common/Layout";


class AboutPage extends Component {

    render() {
        return (
            <div>
                <Layout inner={About} {...this.props}/>
            </div>
        );
    }
}

export default AboutPage;