
import React, { Component } from 'react';
import { compose, withProps } from "recompose";
import { withGoogleMap, GoogleMap, Marker } from "react-google-maps"; // withScriptjs,
import { connect } from "react-redux";
import { setLocation, clearResult, clearErrors } from "../../actions";


const MapComponent = compose(
    withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyC5poCPZvuhMcqv3NzmUviR521XFT3e9FE&v=3.exp&libraries=geometry,drawing,places",
        loadingElement: <div style={{height: `100%`}}/>,
        containerElement: <div style={{height: `100vh`}}/>,
        mapElement: <div style={{height: `100%`}}/>
    }),
   // withScriptjs,
    withGoogleMap

)((props) =>

    <GoogleMap
        defaultZoom={12}
        defaultCenter={{lat: 46.408683, lng: 30.74}}
        onClick={(event) => props.onMapClick(event)}
       >
        {props.marks.map((item, index) => {
            return(
                <Marker
                    key={index}
                    position={{ lat: +item.lat, lng: +item.lng }}
                />
            )
        }
        )}
    </GoogleMap>
);


class Map extends Component {

    constructor(props) {
        super(props);
        this.state = {
            markers: [],
            allowedMarker: this.props.allowedMarker
        };
        this.mapClicked = this.mapClicked.bind(this);
        this.mapGeocoder = this.mapGeocoder.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            if (this.state.allowedMarker) {
                this.setState({
                    markers: nextProps.marker.concat(nextProps.locations)
                });
            } else {
                this.setState({
                    markers: nextProps.pins
                });
            }
        }
    }

    mapGeocoder (latLng){
        const user = this.props.authentication.user;
        const geocoder  = new window.google.maps.Geocoder();
        const _this = this;
        geocoder.geocode({'location':  latLng}, function(results, status) {
            if (status === 'OK') {
                if (results[1]) {
                    let {lat, lng} = latLng;
                    const dataLocation = {
                        id: '',
                        name: results[0].formatted_address,
                        lat: lat(),
                        lng: lng(),
                        userId: user ? user.userId : '',
                        type: ''
                    };
                    _this.props.setLocation(dataLocation);
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }

    mapClicked(mapProps) {
        this.mapGeocoder(mapProps.latLng);
        this.props.clearResult();
        this.props.clearErrors();
    }

    render() {
        return (
            <MapComponent
                onMapClick={this.mapClicked}
                marks={this.state.markers}
                {...this.props}
            />
        )
    }
}

const mapStateToProps = store => ({
    pins: store.user[0] ? store.user[0].Locations : [],
    marker: store.map ? store.map : [],
    locations: store.locations? store.locations : [],
    authentication: store.authentication
});

const mapDispatchToProps = dispatch => ({
    setLocation: (dataLocation) => dispatch(setLocation(dataLocation)),
    clearResult: () => dispatch(clearResult()),
    clearErrors: () => dispatch(clearErrors())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Map);
