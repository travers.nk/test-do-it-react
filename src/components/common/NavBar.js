import React, {Component} from 'react';
import {connect} from "react-redux";
import { Navbar, Nav, NavItem } from 'react-bootstrap';

class NavBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loggedUser: false,
        };
    }

    componentDidMount(){
        if (this.props.loggedUser.loggedIn){
            this.setState({
                loggedUser: true
            });
        } else this.setState({
            loggedUser: false
        });
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            if (nextProps.loggedUser.loggedIn){
                this.setState({
                    loggedUser: true
                });
            } else this.setState({
                loggedUser: false
            });
        }
    }

    render(){
        return (
            <Navbar>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="#">Maps</a>
                    </Navbar.Brand>
                </Navbar.Header>
                <Nav>
                    <NavItem eventKey={2} href="/">Users List Page</NavItem>
                    <NavItem eventKey={1} href="/user-page">User Page</NavItem>
                    <NavItem eventKey={3} href="/about">About</NavItem>
                </Nav>
                <Nav pullRight>
                    { !this.state.loggedUser
                        && <NavItem eventKey={4} href="/login"> Sign In / Sign Up </NavItem> }
                    { this.state.loggedUser
                        && <NavItem eventKey={5} href="/login"> Logout </NavItem> }
                </Nav>
            </Navbar>
        )
    }
}

const mapStateToProps = store => ({
    loggedUser: store.authentication
});

export default connect(
    mapStateToProps,
)(NavBar);