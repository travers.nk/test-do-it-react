import React from "react";
import { Row, Grid } from 'react-bootstrap';

class About extends React.Component {
    render() {
        return (
            <Grid>
            <Row className="show-grid">
            <div>
                <h3> Nataliia Kulychyk </h3>
                <h4>OBJECTIVE</h4>
                <p>To get a full-stack web developer position in IT company </p>
                <p>where my skills in developing and maintaining functional websites will be used.</p>
                <p>Learn new technologies and get new experience as well.</p>
                <h4>SKILLS</h4>
                <ul>
                    <li> Languages
                        <ul>
                            <li>Python, Javascript, PHP</li>
                        </ul>
                    </li>
                    <li> Frameworks
                        <ul>
                            <li>Django, Meteor, Nodejs, Laravel, Django REST Framework</li>
                            <li>React, Angular</li>
                        </ul>
                    </li>
                    <li> DBMS
                        <ul>
                            <li>PostgreSQL, MySQL, MongoDB, MSSQL Server, Redis</li>
                        </ul>
                    </li>
                    <li> Additional technologies
                        <ul>
                            <li>Git, Vagrant, uWSGI, Nginx, Apache, jQuery HTML/CSS, JSON</li>
                        </ul>
                    </li>
                    <li> OS
                        <ul>
                            <li>Ubuntu, macOS, MS Windows</li>
                        </ul>
                    </li>
                </ul>
                <h4> EXPERIENCE </h4>
                <ul>
                    <li><h5>September 2016 – Present 1000Geeks, <b>Full-stack Web Developer</b></h5>
                        <ul>
                            <li><h5><b>Hybrid mobile application for existing e-commerce project based on PrestaShop</b></h5>
                                <p> Mobile application has its own backend, based on Meteor</p>
                                <p>Challenges:</p>
                                <p> - user synchronization</p>
                                <p> - user shop cart synchronization</p>
                                <p> - products catalog transformation and synchronization</p>
                                <p> Solution: </p>
                                <p> - products update hook for the Prestashop, sending new info about product to the sync. service </p>
                                <p> - synchronization service to transform and update products catalog based on Laravel,
                                    synchronization Mysql and MongoDB</p>
                                <p> - user and user cart synchronization via PrestaShop API</p>
                                <p>The main technologies of the project — Meteor Framework, Cordova, MongoDB, React, Laravel</p>
                                <p>Application was deployed with forever and Nginx.</p>
                            </li>
                            <li><h5><b>Application for managing  and sharing medical personal information using bracelets with QR-code</b></h5>
                                <p>The main technologies of the project — Meteor Framework, MongoDB, jQuery, Handlebars</p>
                                <p>Application was deployed with forever and Nginx.</p>
                            </li>
                            <li><h5><b> API Service for communicating between smart-contracts(Ethereum) and ERP</b></h5>
                                <p>The main technologies of the project — Nodejs, web3(js)</p>
                                <p>Application was deployed with npm2 and Nginx.</p>
                            </li>
                        </ul>
                    </li>
                    <li><h5>February  2015 – Present, Freelancee, <b>Web Developer</b></h5>
                        <ul>
                            <li> <h5><b>Service for looking for spare parts</b></h5>
                                <p>The main technologies of the project — Django, JQuery, JavaScript, socket.io</p>
                                <p>Application was deployed with uwsgi and Nginx.</p>
                            </li>
                        </ul>
                    </li>
                    <li><h5>August 2003 — August 2010, Foxtrot-Service, <b> Head of Business Automation Departament</b></h5>
                        <ul>
                            <li><h5>Business process analysis, making 1C-based automation for accounting,</h5>
                                <p>inventory management and various processes in the company.</p>
                                <p>Setting up a QA framework for the software both from outside and created internally,</p>
                                <p>to conform the business requirements and internal standards.</p>
                            </li>
                        </ul>
                    </li>
                </ul>
                <h4>EDUCATION</h4>
                <ul>
                    <li> Intersog Full-stack Node.js, Angular, 2017</li>
                    <li>Intersog BackEnd PHP, Python 2016</li>
                    <li>BinaryStudio Academy Full-stack PHP, Backbone, Marionette, 2016</li>
                    <li><p>Sevastopol State Technical University</p>
                        <p>Faculty: Computer Intellectual Systems</p>
                        <p>Qualified as: Systems Engineer</p></li>
                </ul>
                <h4>English, Intermediate Level</h4>
            </div>
            </Row>
            </Grid>
        );
    }
}

export default About
