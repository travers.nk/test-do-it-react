import React, {Component} from 'react';
import { Panel } from 'react-bootstrap';

class InfoPanel extends Component {

    constructor (props) {
        super(props);
        this.getValidationState = this.getValidationState.bind(this);
    };

    getValidationState() {
        const success = this.props.success;
        if (success === 'success') {
            return 'success'
        } else if (success === '' || !success ) {
            return 'info'
        } else if (success !== 'success') return 'warning';
    }

    render() {
        const panelStyle = this.getValidationState();
        const errors = this.props.errors.message ? this.props.errors.message : [] ;

        return (
            <div>
                <Panel header='Status' bsStyle={panelStyle}>
                    {this.props.success}
                    <ul>
                        {errors.map((error, index) => (
                            <li key={index}>{error}</li>
                        ))}
                    </ul>
                </Panel>
            </div>
        )
    }
}

export default InfoPanel;