import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { login, logout, clearResult, clearErrors } from '../../actions/index';
import InfoPanel from "../../components/common/InfoPanel";


class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            submitted: false,
            success: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.props.logout();
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps){
            this.setState({
                success: nextProps.result
            });
        }
    }

    handleChange(event) {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({submitted: true});
        const {name, password} = this.state;
        this.props.login({
            'name': name,
            'password': password
        })
    }

    handleClick(event) {
        this.props.clearResult();
        this.props.clearErrors();
    }

    render() {
        const { name, password, submitted } = this.state;
        return (
            <div className="col-md-6 col-md-offset-3">
                <h2>Login</h2>

                <InfoPanel success={this.state.success} errors={this.props.errors} />

                <form name="form" onSubmit={this.handleSubmit} onClick={this.handleClick}>
                    <div className={'form-group' + (submitted && !name ? ' has-error' : '')}>
                        <label htmlFor="name">Name</label>
                        <input type="text"
                               className="form-control"
                               name="name"
                               value={name ? name : ''}
                               onChange={this.handleChange} />

                        {submitted && !name &&
                            <div className="help-block">Username is required</div>
                        }

                        </div>
                    <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
                        <label htmlFor="password">Password</label>
                        <input type="password"
                               className="form-control"
                               name="password"
                               value={password}
                               onChange={this.handleChange} />

                        {submitted && !password &&
                            <div className="help-block">Password is required</div>
                        }

                    </div>

                    <div className="form-group">
                        <button className="btn btn-primary">Login</button>
                        <Link to="/register" className="btn btn-link">Register</Link>
                    </div>

                </form>
            </div>
        );
    }
}

const mapStateToProps = store => ({
    errors: store.errors,
    result: store.result,
});

const mapDispatchToProps = dispatch => ({
    login: (user, password) => dispatch(login(user, password)),
    logout: () => dispatch(logout()),
    clearResult: () => dispatch(clearResult()),
    clearErrors: () => dispatch(clearErrors())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);