import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { register, clearResult, clearErrors } from '../../actions/index';
import InfoPanel from "../../components/common/InfoPanel";


class Register extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {
                name: '',
                age: '',
                occupation: '',
                password: ''
            },
            submitted: false,
            success: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            this.setState({
                success: nextProps.result
            });

        }}

    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({ submitted: true });
        const { user } = this.state;
        this.props.register(user);

    }

    handleClick(event) {
        this.props.clearResult();
        this.props.clearErrors();
    }

    render() {
        const { user, submitted } = this.state;
        return (
            <div className="col-md-6 col-md-offset-3">
                <h2>Register</h2>

                <InfoPanel success={this.state.success} errors={this.props.errors} />

                <form name="form" onSubmit={this.handleSubmit} onClick={this.handleClick}>
                    <div className={'form-group' + (submitted && !user.name ? ' has-error' : '')}>
                        <label htmlFor="name">Name</label>
                        <input type="text"
                               className="form-control"
                               name="name" value={user.name}
                               onChange={this.handleChange} />
                        {submitted && !user.name &&
                            <div className="help-block">Name is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.age ? ' has-error' : '')}>
                        <label htmlFor="age">Age</label>
                        <input type="text"
                               className="form-control"
                               name="age"
                               value={user.age}
                               onChange={this.handleChange} />
                        {submitted && !user.age &&
                            <div className="help-block">Age is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.occupation ? ' has-error' : '')}>
                        <label htmlFor="occupation">Occupation</label>
                        <input type="text"
                               className="form-control"
                               name="occupation"
                               value={user.occupation}
                               onChange={this.handleChange} />
                        {submitted && !user.occupation &&
                            <div className="help-block">Occupation is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.password ? ' has-error' : '')}>
                        <label htmlFor="password">Password</label>
                        <input type="password"
                               className="form-control"
                               name="password"
                               value={user.password}
                               onChange={this.handleChange} />
                        {submitted && !user.password &&
                            <div className="help-block">Password is required</div>
                        }
                    </div>
                    <div className="form-group">
                        <button className="btn btn-primary">Register</button>
                        <Link to="/login" className="btn btn-link">Cancel</Link>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = store => ({
    errors: store.errors,
    result: store.result,
});

const mapDispatchToProps = dispatch => ({
    register: (data) => dispatch(register(data)),
    clearResult: () => dispatch(clearResult()),
    clearErrors: () => dispatch(clearErrors())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Register);