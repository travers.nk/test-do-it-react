import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {fetchUserLocation} from "../../actions";

class User extends Component {

    constructor (props) {
        super(props);
        this.userClick = this.userClick.bind(this);
    };

    userClick(e){
        this.props.fetchUserLocation(this.props.id);
    }

    render(){
        return (
            <li>
                <a href ="#" onClick={this.userClick}> {this.props.name} </a>
            </li>
        )
    }
}

User.propTypes = {
    name: PropTypes.string.isRequired
};


const mapStateToProps = store => ({
//   locations: store.user[0] ? store.user[0].Locations : []
});

const mapDispatchToProps = dispatch => ({
    fetchUserLocation: (userId) => dispatch(fetchUserLocation(userId))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(User);

