import React, {Component} from 'react';
import User from './User';
import {connect} from "react-redux";
import {fetchUsers} from '../../actions/index';

class UsersList extends Component {
    componentDidMount() {
        this.props.fetchUsers();
    }

    render() {
        const users = this.props.users || [];
        return (
            <ul>
                {users.map((user, index) => (
                    <User key={index} {...user}/>
                ))}
            </ul>
        )
    }
}

const mapStateToProps = store => ({
    users: store.users
});

const mapDispatchToProps = dispatch => ({
    fetchUsers: () => dispatch(fetchUsers())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UsersList);
