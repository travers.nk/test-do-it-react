import React, {Component} from 'react';
import { Row, Col, Grid } from 'react-bootstrap';
import UsersList from "../users/UsersList";
import Map from "../common/Map";
import { connect } from 'react-redux';
import InfoPanel from "../../components/common/InfoPanel";

class Main extends Component {
    constructor (props) {
        super(props);
        this.state = {
            success: ''
        };
    };

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps){
            this.setState({
                success: nextProps.result
            });
        }
    }

    render() {
        return (
            <Grid>
                <Row className="show-grid">
                    {this.state.success && <InfoPanel success={this.state.success} errors={this.props.errors} />}
                    <Col xs={6} md={4}> <UsersList/> </Col>
                    <Col xs={12} md={8}> <Map allowedMarker={false}/> </Col>
                </Row>
            </Grid>
        );
    }
}

const mapStateToProps = store => ({
    result: store.result,
    errors: store.errors,
});

export default connect(
    mapStateToProps,
)(Main);