import React, {Component} from 'react';
import {fetchUsers} from "../actions";
import UserProfile from "./user/UserProfile";
import Layout from "./common/Layout";


class UserPage extends Component {
    componentDidMount() {
        fetchUsers();
    }

    render() {
        return (
            <div>
                <Layout inner={UserProfile} {...this.props}/>
            </div>
        );
    }
}

export default UserPage;
