import React, {Component} from 'react';
import { Row, Col, Grid } from 'react-bootstrap';
import AddLocation from "../../containers/AddLocation";
import Map from "../common/Map";
import Locations from "./Locations";
import {fetchUsers} from "../../actions";
import {connect} from "react-redux";


class UserProfile extends Component {
    componentDidMount() {
        fetchUsers();
    }

    render() {
        return (
            <Grid>
                <Row className="show-grid">
                    <Col xs={6} md={4}>
                    <Col> <AddLocation/> </Col>
                    <Col> <Locations locations={this.props.locations}/> </Col>
                    </Col>
                    <Col xs={12} md={8}> <Map allowedMarker={true}/> </Col>
                </Row>
            </Grid>
        );
    }
}

const mapStateToProps = store => ({
    locations: store.locations? store.locations : []
});

export default connect(
    mapStateToProps,
)(UserProfile);