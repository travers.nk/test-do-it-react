import React, {Component} from 'react';

class Locations extends Component {

    render() {
        const locations = this.props.locations || [];
        return (
            <ul>
                {locations.map((location, index) => (
                    <li key={index}>{location.name}</li>
                ))}
            </ul>
        )
    }
}

export default Locations