import React from 'react';
import {Route, Switch} from "react-router";
import { history } from '../actions/history';
import NotFound from "./common/NotFound";
import UserPage from "./UserPage";
import MainPage from "./MainPage";
import LoginPage from "./LoginPage";
import RegisterPage from "./RegisterPage";
import AboutPage from "./AboutPage";
import { PrivateRoute } from '../components/common/PrivateRoute';

const App = () => (
    <Switch history={ history } >
        <Route exact path='/' component={MainPage}/>
        <Route path='/login' component={LoginPage}/>
        <Route path="/register" component={RegisterPage} />
        <Route path="/about" component={AboutPage} />
        <PrivateRoute path='/user-page' component={UserPage}/>
        <Route path="*" component={NotFound}/>
    </Switch>
);

export default App
