import React, {Component} from 'react';
import Main from "./users/Main";
import Layout from "./common/Layout";


class MainPage extends Component {

    render() {
        return (
            <div>
                <Layout inner={Main} {...this.props}/>
            </div>
        );
    }
}

export default MainPage;