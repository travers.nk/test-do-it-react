import React, {Component} from 'react';
import Login from "./auth/Login";
import Layout from "./common/Layout";


class LoginPage extends Component {

    render() {
        return (
            <div>
                <Layout inner={Login} {...this.props}/>
            </div>
        );
    }
}

export default LoginPage;