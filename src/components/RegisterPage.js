import React, {Component} from 'react';
import Register from "./auth/Register";
import Layout from "./common/Layout";


class RegisterPage extends Component {

    render() {
        return (
            <div>
                <Layout inner={Register} {...this.props}/>
            </div>
        );
    }
}

export default RegisterPage;